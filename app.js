const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/assets/templates/index.html');
});

app.use(express.static(__dirname + '/assets'));

let socket_list = {};
let player_list = {};

let Player = (id) => {
    let player = {
        number: Math.floor((Math.random() * 9)),
        x: Math.floor((Math.random() * 700)),
        y: Math.floor((Math.random() * 700)),
        id: id,
        pressingRight: false,
        pressingLeft: false,
        pressingUp: false,
        pressingDown: false,
        speed: 10,
	};

    player.updatePosition = () => {
        if(player.pressingRight)
            player.x += player.speed;
        if(player.pressingLeft)
            player.x -= player.speed;
        if(player.pressingUp)
            player.y -= player.speed;
        if(player.pressingDown)
            player.y += player.speed;
	}

    return player;
}

io.on('connection', (socket) => {
	console.log('user connected: ', socket.id);
	socket_list[socket.id] = socket;

	let player = Player(socket.id);
    player_list[socket.id] = player;

	socket.on('disconnect', () => {
		console.log('user disconnected: ', socket.id );
		delete socket_list[socket.id];
		delete player_list[socket.id];
	});

	socket.on('keyPress', (data) => {
        if(data.inputId === 'left')
            player.pressingLeft = data.state;
        else if(data.inputId === 'right')
            player.pressingRight = data.state;
        else if(data.inputId === 'up')
            player.pressingUp = data.state;
        else if(data.inputId === 'down')
            player.pressingDown = data.state;
    });

	socket.on('client_message', (data) => {
		console.log('Client says: ', data.msg);
	});

	socket.emit('server_message', { msg: 'connection OK' });
});

server.listen(3000, () => {
	console.log('Server on 3000');
});

setInterval(() => {
	let positions = [];
	for(let i in player_list) {
		let player = player_list[i];
		player.updatePosition();
		positions.push({
            x: player.x,
            y: player.y,
            number: player.number
		});
	}

	for(let i in socket_list) {
		let socket = socket_list[i];
		socket.emit('new_positions', positions);
	}
}, 1000/30);